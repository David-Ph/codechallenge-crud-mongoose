const request = require("supertest");
const faker = require("faker");
const jwt = require("jsonwebtoken");
const app = require("../index");
const { good, customer, user } = require("../models");
let data = [];
let userToken = "";
let adminToken = "";

beforeAll(async () => {
  data = await Promise.all([good.find(), customer.find()]);

  // create user and admin
  const employee = await user.create({
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: "Aneh123!!",
  });

  const admin = await user.create({
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: "Aneh123!!",
    role: "admin",
  });
  // create a token based off that user or admin
  userToken = jwt.sign({ user: employee._id }, process.env.JWT_SECRET);
  adminToken = jwt.sign({ user: admin._id }, process.env.JWT_SECRET);
});

describe("/transactions POST", () => {
  it("Transactions must be created", async () => {
    const response = await request(app)
      .post("/transactions")
      .set("Authorization", `Bearer ${adminToken}`) // set the token in the test
      .send({
        id_good: data[0][Math.floor(Math.random() * data[0].length)]._id,
        id_customer: data[1][Math.floor(Math.random() * data[1].length)]._id,
        quantity: "10",
      });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toBeInstanceOf(Object);
  });

  it("id_good not valid", async () => {
    const response = await request(app)
      .post("/transactions")
      .set("Authorization", `Bearer ${adminToken}`)
      .send({
        id_good: "asdwqdsadqwe",
        id_customer: data[1][Math.floor(Math.random() * data[1].length)]._id,
        quantity: "10",
      });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
  });

  it("id_customer not valid", async () => {
    const response = await request(app)
      .post("/transactions")
      .set("Authorization", `Bearer ${adminToken}`)
      .send({
        id_good: data[0][Math.floor(Math.random() * data[0].length)]._id,
        id_customer: "sadqwesad",
        quantity: "10",
      });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
  });

  it("id_customer and id_good not valid", async () => {
    const response = await request(app)
      .post("/transactions")
      .set("Authorization", `Bearer ${adminToken}`)
      .send({
        id_good: "asdwqdsad",
        id_customer: "sadqwesad",
        quantity: "10",
      });
    expect(response.statusCode).toEqual(400);
    expect(response.body).toBeInstanceOf(Object);
  });
});

describe("/transactions GET", () => {
  it("Transactions must exists", async () => {
    const response = await request(app)
      .get("/transactions")
      .set("Authorization", `Bearer ${userToken}`);

    expect(response.statusCode).toEqual(200);
    expect(response.body).toBeInstanceOf(Object);
  });
});
