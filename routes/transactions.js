const express = require("express");
const router = express.Router();

// ? import auth
// //////////////
const { admin, user, adminOrUser } = require("../middlewares/auth");

// ? import validator
// /////////////////
const {
  createOrUpdateTransactionValidator,
  getDetailValidator,
} = require("../middlewares/validators/transactions");

// ? import controller
// //////////////////
const {
  createTransaction,
  getAllTransactions,
  getDetailTransaction,
  updateTransaction,
  deleteTransaction,
} = require("../controllers/transactions");

// ? make routes
// ////////////////
// router.get("/", user, getAllTransactions);
// router.get(
//   "/:id",
//   adminOrUser,
//   getDetailTransactionValidator,
//   getDetailTransaction
// );
// router.post("/", admin, createOrUpdateTransactionValidator, createTransaction);
// router.put(
//   "/:id",
//   admin,
//   createOrUpdateTransactionValidator,
//   updateTransaction
// );
// router.delete("/:id", admin, deleteTransaction);
router.get("/", adminOrUser, getAllTransactions);
router.get("/:id", adminOrUser, getDetailValidator, getDetailTransaction);
router.post("/", admin, createOrUpdateTransactionValidator, createTransaction);
router.put(
  "/:id",
  admin,
  createOrUpdateTransactionValidator,
  updateTransaction
);
router.delete("/:id", admin, deleteTransaction);

module.exports = router;
