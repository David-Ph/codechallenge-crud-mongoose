const express = require("express");
const router = express.Router();

const { supplierValidator } = require("../middlewares/validators/suppliers");

const Supplier = require("../controllers/suppliers");

router.get("/", Supplier.getAllSupplier);
router.get("/:id", Supplier.getOneSupplier);
router.post("/", supplierValidator, Supplier.createSupplier);
router.put("/:id", supplierValidator, Supplier.updateSupplier);
router.delete("/:id", Supplier.deleteSupplier);

module.exports = router;
