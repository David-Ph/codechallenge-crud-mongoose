const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
const bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
      //   ketika orang menginput password
      // akan memanggil setPassword
      //   dan mengoper password yang diinput ke function setPassword sebagai argumen
      set: setPassword,
    },
    role: {
      type: String,
      enum: ["admin", "user"], // this means that role can only have value of either admin or user, otherwise it won't be allowed into the database
      required: true,
      default: "user",
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

function setPassword(password) {
  // ini adalah suatu method untuk encrypt password
  //   hashSync(passwordYangInginDiEncrpyt, berapaKaliDiEncrypt)
  return bcrypt.hashSync(password, 10);
}

userSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("user", userSchema);
