const jwt = require("jsonwebtoken");

// this is to handle getting token for both sign in and sign up
class Auth {
  getToken(req, res, next) {
    try {
      const data = {
        // req.user is received from the previous middlewares callback
        user: req.user._id,
      };
      // jwt.sign will create a token based on data provided
      const token = jwt.sign(data, process.env.JWT_SECRET);
      // then return that token back
      res.status(200).json({ token });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new Auth();
