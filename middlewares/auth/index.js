const passport = require("passport"); // import passport
const LocalStrategy = require("passport-local").Strategy; // login but not using google/facebook login, but we'll be the one handling it
const bcrypt = require("bcrypt"); // to compare the password
const JWTstrategy = require("passport-jwt").Strategy; // to enable jwt in passport
const ExtractJWT = require("passport-jwt").ExtractJwt; // to extract or read jwt
const { user } = require("../../models");

// THIS IS TO HANDLE SIGNUP
exports.signup = (req, res, next) => {
  // if session is true, the postman/user will receive a cookie, which will
  //   session is usually stored with html
  //   session can't be used with mobile
  // that's why we use JWT

  // this will call the passport.use('signup') below, then it will receive the err, user, and info
  // from what they get from done() callback below
  passport.authenticate("signup", { session: false }, (err, user, info) => {
    if (err) {
      return next({ message: err.message, statusCode: 401 });
    }

    if (!user) {
      return next({ message: info.message, statusCode: 401 });
    }

    // set a variable in req
    // and store the user in it
    req.user = user;

    next();
  })(req, res, next);
};

passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    // you can just focus on code below
    async (req, email, password, done) => {
      try {
        // create user based on req.body
        const data = await user.create(req.body);
        // if done, pass back the err, data, and messages back to the passport.authenticate above
        return done(null, data, { message: "User can be created" });
      } catch (e) {
        return done(e, false, { message: "User can't be created" });
      }
    }
  )
);

// Logic to signin
exports.signin = (req, res, next) => {
  passport.authenticate("signin", { session: false }, (err, user, info) => {
    if (err) {
      return next({ message: err.message, statusCode: 401 });
    }

    if (!user) {
      return next({ message: info.message, statusCode: 401 });
    }

    req.user = user;

    next();
  })(req, res, next);
};

passport.use(
  "signin",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, email, password, done) => {
      try {
        const data = await user.findOne({ email });

        if (!data) {
          return done(null, false, { message: "User is not found!" });
        }

        const validate = await bcrypt.compare(password, data.password);

        if (!validate) {
          return done(null, false, { message: "Wrong password!" });
        }

        return done(null, data, { message: "Login success!" });
      } catch (e) {
        return done(e, false, { message: "User can't be created" });
      }
    }
  )
);

// Logic to admin
exports.admin = (req, res, next) => {
  passport.authenticate("admin", { session: false }, (err, user, info) => {
    if (err) {
      return next({ message: err.message, statusCode: 403 });
    }

    if (!user) {
      return next({ message: info.message, statusCode: 403 });
    }

    req.user = user;

    next();
  })(req, res, next);
};

passport.use(
  "admin",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const data = await user.findOne({ _id: token.user });

        if (data.role === "admin") {
          return done(null, token);
        }

        return done(null, false, { message: "Forbidden Access" });
      } catch (error) {
        return done(e, false, { message: "Forbidden access" });
      }
    }
  )
);

// Logic for user
exports.user = (req, res, next) => {
  passport.authenticate("user", { session: false }, (err, user, info) => {
    if (err) {
      return next({ message: err.message, statusCode: 403 });
    }

    if (!user) {
      return next({ message: info.message, statusCode: 403 });
    }

    req.user = user;

    next();
  })(req, res, next);
};

passport.use(
  "user",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const data = await user.findOne({ _id: token.user });

        if (data.role === "user") {
          return done(null, token);
        }

        return done(null, false, { message: "Forbidden Access" });
      } catch (error) {
        return done(e, false, { message: "Forbidden access" });
      }
    }
  )
);

// Logic to adminOrUser
exports.adminOrUser = (req, res, next) => {
  passport.authenticate(
    "adminOrUser",
    { session: false },
    (err, user, info) => {
      if (err) {
        return next({ message: err.message, statusCode: 403 });
      }

      if (!user) {
        return next({ message: info.message, statusCode: 403 });
      }

      req.user = user;

      next();
    }
  )(req, res, next);
};

passport.use(
  "adminOrUser",
  new JWTstrategy(
    {
      secretOrKey: process.env.JWT_SECRET,
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    },
    async (token, done) => {
      try {
        const data = await user.findOne({ _id: token.user });

        if (data.role === "admin" || data.role === "user") {
          return done(null, token);
        }

        return done(null, false, { message: "Forbidden Access" });
      } catch (error) {
        return done(e, false, { message: "Forbidden access" });
      }
    }
  )
);
